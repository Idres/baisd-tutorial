def selection_sort(data: list) -> list:
    sorted_data = data.copy()
    n = len(sorted_data)
    for i in range(n):
        max_index = i
        for j in range(i + 1, n):
            if sorted_data[max_index] > data[j]:
                sorted_data[max_index], sorted_data[j] = (
                    sorted_data[j],
                    sorted_data[max_index],
                )

    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("Enter 10 number: ").split()))
    sorted_data = selection_sort(data)
    print(sorted_data)
