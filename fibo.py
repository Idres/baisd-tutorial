def Fibo(n):
    a, b = 0, 1
    d = "0, 1"
    d_list = [0, 1]
    for i in range(n - 1):
        c = a + b
        a, b = b, c
        d = d + (", {}".format(c))
        d_list.append(c)

    return d


def Fibonacci(n):
    if n <= 0:
        print("Incorrect input")

    elif n == 1:
        return 0

    elif n == 2:
        return 1
    else:
        return Fibonacci(n - 1) + Fibonacci(n - 2)


def bottom(n):
    f0 = 0
    f1 = 1
    f2 = 1

    for i in range(2, n + 1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":
    # print(Fibo(int(input("Input number of Fibonacci number: "))))
    # print(Fibonacci(int(input(':'))))
    print(bottom(int(input(":"))))
    pass
