def re_change(n):
    ten = 0
    five = 0
    four = 0
    three = 0
    two = 0
    one = 0

    if n / 10 >= 1:
        ten = n // 10
        back = n % 10
        if back != 0:
            five = back // 5
            back5 = back % 5
            if back != 0:
                four = back5 // 4
                back4 = back % 4
                if back4 != 0:
                    three = back4 // 3
                    back3 = back4 % 3
                    if back3 != 0:
                        two = back3 // 3
                        back2 = back3 % 2
                        if back2 != 0:
                            one = back2

    return [ten, five, four, three, two, one]


def change_new(n):
    coins = [10, 5, 4, 3, 2, 1]
    back_coins = [0, 0, 0, 0, 0, 0]
    for i in range(len(coins)):
        if n != 0:
            back_coins[i] = n // coins[i]
            n = n % coins[i]
    return back_coins


if __name__ == "__main__":
    # print(re_change(int(input("->"))))
    print(change_new(int(input("->"))))
